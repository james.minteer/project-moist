We need to have a uniformed policy for naming of everything

Here is what i have come up with so far:

Files-
    All upercase, spaces
    example: "First Second Third.txt"
    exception: "main.cpp"

Branches-
    All uppercase, underscores
    example: "First_Second_Third"
    exception: "master"

Classes-
    All uppercase, underscores
    example: "class First_Second_Third"

Functions-
    All upercase, no spaces
    example: "void FirstSecondThird();"

Objects-
    First undercase, underscores
    example: "Node first_Second_Third;"

Primitive Variables-
    First undercase, no spaces
    example: "char * firstSecondThird

Note:
-The classes and branch naming is the same, but i cant think of how to fix that
-Classes and names for objects are similar, which is double edged
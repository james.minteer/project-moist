#pragma once
#include <SFML\Graphics.hpp>
#include <cmath>
//diameters will be 120 * sqrt(3)

//z=-x-y
									//plains                 desert                  ocean                     forest                  hills
static sf::Color biome_Colors[] = { sf::Color(177, 226, 86), sf::Color(250,255,186), sf::Color(124, 218, 255), sf::Color(74, 104, 74), sf::Color(145, 127, 98)};

class Tile {
private:
	sf::Vector2f i;
	sf::Vector2f j;
	sf::Vector2f x;
	sf::Vector2f y;
	int seed;
	sf::CircleShape hex;
	sf::Text label;
	sf::Font oblivious;
	sf::Color calculateBiome();
	int biome;
public:
	~Tile() {
		//i dont think i need to close thhe font file if i just let the compiler deconstruct the tile object
	}
	Tile(int newSeed, float newSimpleX, float newSimpleY, sf::Vector2f offset) : simpleX(newSimpleX), simpleY(newSimpleY) {
		i = sf::Vector2f(180.f, 0.f);
		j = sf::Vector2f(90.f, 90.f * sqrt(3));
		x = simpleX * i;
		y = simpleY * j;
		seed = newSeed;

		hex = sf::CircleShape(60.f * sqrt(3), 6);
		hex.setOrigin(hex.getPosition() + sf::Vector2f(hex.getRadius(), hex.getRadius()));
		hex.setPosition(x + y + offset);
		hex.setOutlineColor(sf::Color::Black);
		hex.setOutlineThickness(5.f);
		hex.setFillColor(calculateBiome());

		oblivious.loadFromFile("ObliviousFont.ttf");

		std::string labelString = std::to_string((int)simpleX) + "," + std::to_string((int)simpleY);
		//labelString += ;
		//labelString += ',';
		//labelString += std::to_string((int)simpleY);
		label.setFont(oblivious);
		label.setCharacterSize(hex.getRadius() / 5);
		label.setColor(sf::Color::Black);
		label.setString(labelString);
		label.setOrigin(label.getPosition() + (sf::Vector2f(label.getCharacterSize() * 3 / 4, label.getCharacterSize() / 2)));
		label.setPosition(hex.getPosition());
	}
	sf::Vector2f getPosition() {
		return hex.getPosition();
	}
	const float simpleX;
	const float simpleY;
	void draw(sf::RenderWindow & window_Map) {
		window_Map.draw(this->hex);
		window_Map.draw(this->label);
	}
	void printInfo();
};

sf::Color Tile::calculateBiome() {
	this-> biome = ((int)sqrt(sqrt(pow((int)simpleX, 2) + pow((int)simpleY, 2) * pow((int)(simpleX+simpleY),2) * pow(seed % 1000,2)))) % 5;//this is some thing that has been floating around in my head and i think i learned the whole square root thing from a vsauce video 2 years ago
	return biome_Colors[biome];
}

void Tile::printInfo() {
	std::cout << "Location: (" << (int)simpleX << "," << (int)simpleY << ")" << std::endl;
	std::cout << "Biome: " << biome << ": ";
	switch (biome) {
	case 0:
		std::cout << "Plains";
		break;
	case 1:
		std::cout << "Desert";
		break;
	case 2:
		std::cout << "Ocean";
		break;
	case 3:
		std::cout << "Forest";
		break;
	case 4:
		std::cout << "Hills";
		break;
	}

	std::cout << std::endl;
}
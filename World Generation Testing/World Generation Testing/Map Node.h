#pragma once
#include "Tile.h"
#include <SFML\Graphics.hpp>
#include <iostream>


class Map_Node {
private:
	
	//Map_Node * trunkNodeFront;//+i
	//Map_Node * trunkNodeBack;//-i
	//Map_Node * branchUpNode; //+j
	//Map_Node * branchDownNode; //-j
	const sf::Vector2f worldSize;
protected:
	Tile tile;
public:
	Map_Node(sf::Vector2f worldSize, int seed, float simpleX, float simpleY, sf::Vector2f offset) : worldSize(worldSize), tile(seed, simpleX, simpleY, offset) {
		//std::cout << "New tile located at (" << tile.simpleX << "," << tile.simpleY << ")" << std::endl;
	}
	bool isInView();
	void drawTile(sf::RenderWindow & window_Map){
		tile.draw(window_Map);
	}
	virtual ~Map_Node() = 0{
		
	}
};



bool Map_Node::isInView() {
	//std::cout << std::endl << "tile.getPosition().x: " << tile.getPosition().x << "  worldSize.x:  " << worldSize.x << std::endl;
	//std::cout << std::endl << "tile.getPosition().y: " << tile.getPosition().y << "  worldSize.y:  " << worldSize.y << std::endl << std::endl;
	return (tile.getPosition().x >= 0 && tile.getPosition().x <= worldSize.x && tile.getPosition().y >= 0 && tile.getPosition().y <= worldSize.y);
}



class Up_Down_Map_Node : public Map_Node {
private:
	Up_Down_Map_Node * Up_Down;
public:
	Up_Down_Map_Node(bool isUp, sf::Vector2f worldSize, int seed, float simpleX, float simpleY, sf::Vector2f offset) : Map_Node(worldSize, seed, simpleX, simpleY, offset) {
		if (isInView()) {
			Up_Down = new Up_Down_Map_Node(isUp, worldSize, seed, simpleX, (isUp ? simpleY + 1.f : simpleY - 1.f), offset);
		}
		else {
			Up_Down = nullptr;
		}
	}
	int draw(sf::RenderWindow & window_Map);
	~Up_Down_Map_Node() {
		delete Up_Down;//should delete eveything down the branch
	}

};

int Up_Down_Map_Node::draw(sf::RenderWindow & window_Map) {
	int drawn = 1;
	drawTile(window_Map);
	if (Up_Down == nullptr) {
		drawn += 0;
	}
	else {
		drawn += Up_Down->draw(window_Map);
	}
	return drawn;
}



class Front_Back_Map_Node : public Map_Node {
private:
	Front_Back_Map_Node * Front_Back;
	Up_Down_Map_Node * Up;
	Up_Down_Map_Node * Down;
public:
	Front_Back_Map_Node(bool isFront, sf::Vector2f worldSize, int seed, float simpleX, float simpleY, sf::Vector2f offset) : Map_Node(worldSize, seed, simpleX, simpleY, offset) {
		//std::cout << "trunk component:" << std::endl;
		if (isInView()) {
			Front_Back = new Front_Back_Map_Node(isFront, worldSize, seed, (isFront ? simpleX + 1.f : simpleX - 1.f), simpleY, offset);
			Up = new Up_Down_Map_Node(true, worldSize, seed, simpleX, simpleY + 1.f, offset);
			Down = new Up_Down_Map_Node(false, worldSize, seed, simpleX, simpleY - 1.f, offset);
		}
		else {
			Front_Back = nullptr;
			Up = nullptr;
			Down = nullptr;
		}
	}
	int draw(sf::RenderWindow & window_Map);
	~Front_Back_Map_Node() {
		delete Front_Back;
		delete Up;
		delete Down;
	}
};

int Front_Back_Map_Node::draw(sf::RenderWindow & window_Map) {
	int drawn = 1;
	drawTile(window_Map);

	drawn += (Front_Back == nullptr ? 0 : Front_Back->draw(window_Map));
	drawn += (Up == nullptr ? 0 : Up->draw(window_Map));
	drawn += (Down == nullptr ? 0 : Down->draw(window_Map));

	return drawn;
}

class Root_Map_Node : public Map_Node {
private:
	Front_Back_Map_Node * Front;
	Front_Back_Map_Node * Back;
	Up_Down_Map_Node * Up;
	Up_Down_Map_Node * Down;
public:
	Root_Map_Node(sf::Vector2f worldSize, int seed, sf::Vector2f offset, float simpleX = 0.f, float simpleY = 0.f) : Map_Node(worldSize, seed, simpleX, simpleY, offset) {
		Front = new Front_Back_Map_Node(true, worldSize, seed, simpleX + 1.f, simpleY, offset);
		Back = new Front_Back_Map_Node(false, worldSize, seed, simpleX - 1.f, simpleY, offset);
		Up = new Up_Down_Map_Node(true, worldSize, seed, simpleX, simpleY + 1.f, offset);
		Down = new Up_Down_Map_Node(false, worldSize, seed, simpleX, simpleY - 1.f, offset);
	}
	int draw(sf::RenderWindow & window_Map);
	~Root_Map_Node() {
		delete Front;
		delete Back;
		delete Up;
		delete Down;
	}
	void printInfo(){
		tile.printInfo();
	}
};

int Root_Map_Node::draw(sf::RenderWindow & window_Map) {
	int drawn = 1;
	drawTile(window_Map);

	drawn += (Front == nullptr ? 0 : Front->draw(window_Map));
	drawn += (Back == nullptr ? 0 : Back->draw(window_Map));
	drawn += (Up == nullptr ? 0 : Up->draw(window_Map));
	drawn += (Down == nullptr ? 0 : Down->draw(window_Map));

	return drawn;
}



#pragma once
#include <SFML\Graphics.hpp>
#include "Map Node.h"

class Map {
private:
	const sf::Vector2f i;
	const sf::Vector2f j;

	float mapRootX;
	float mapRootY;
	sf::Vector2f world_Size;
	sf::Vector2f window_Center;
	sf::Vector2f offset;
	Root_Map_Node * root_Map_Node;
	void update_Map();
	const int seed;
public:
	Map(sf::Vector2f new_World_Size, int newSeed, sf::Vector2f new_Window_Center, float newMapRootX, float newMapRootY);
	void updateCenter(float newMapRootX, float newMapRootY);
	int draw(sf::RenderWindow & window_Map) {
		return root_Map_Node->draw(window_Map);
	}
	void printRootInfo() {
		root_Map_Node->printInfo();
	}
};

void Map::update_Map() {
	delete root_Map_Node;
	root_Map_Node = new Root_Map_Node(this->world_Size, this->seed, this->offset, this->mapRootX, this->mapRootY);
}

Map::Map(sf::Vector2f new_World_Size, int newSeed, sf::Vector2f new_Window_Center, float newMapRootX = 0.f, float newMapRootY = 0.f) : seed(newSeed), i(180.f, 0.f), j(90.f, 90.f * sqrt(3)) {
	root_Map_Node = new Root_Map_Node(new_World_Size, newSeed, new_Window_Center);
	this->world_Size = new_World_Size;
	this->window_Center = new_Window_Center;
	this->mapRootX = newMapRootX;
	this->mapRootY = newMapRootY;
}

void Map::updateCenter(float newMapRootX, float newMapRootY) {
	offset = window_Center - (i * newMapRootX) - (j * newMapRootY);
	if (this->mapRootX != newMapRootX || this->mapRootY != newMapRootY) {
		this->mapRootX = newMapRootX;
		this->mapRootY = newMapRootY;
		delete root_Map_Node;
		root_Map_Node = new Root_Map_Node(this->world_Size, this->seed, this->offset, this->mapRootX, this->mapRootY);
	}
}
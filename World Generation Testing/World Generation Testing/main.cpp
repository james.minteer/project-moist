//made with love bu James Minteer


#include <SFML\Graphics.hpp>
#include <cmath>
#include <iostream>
#include "Map.h"

#define worldX 1080
#define worldY 810
//diameters will be 120 * sqrt(3)


int main(void) {

	srand((unsigned int)time(0));

	int seed = ((int)pow(rand(),2) % (99999 - 10000)) + 10000;//a random 5 digit integer
	std::cout << "Seed: " << seed << std::endl;
	std::cout << "The controls are: \n\tArrow keys to move around map\n\tSpace to return to (0,0)" << std::endl;
	system("pause");
	/*sf::Vector2f i(180.f, 0.f);
	sf::Vector2f j(90.f, 90.f * sqrt(3));*/


	sf::RenderWindow window_Map(sf::VideoMode(worldX, worldY), "World Generation");
	sf::Vector2f windowSize(worldX, worldY);

	sf::Vector2f center(window_Map.getSize().x / 2.f , window_Map.getSize().y / 2.f);

	Map map(sf::Vector2f(window_Map.getSize()), seed, center);

	window_Map.setActive(false);
	float mapX = 0;
	float mapY = 0;
	
	bool press = false;//stops user from holding down key

	while (window_Map.isOpen()) {

		sf::Event GameEvent;

		if (window_Map.pollEvent(GameEvent))
		{
			if (GameEvent.type == sf::Event::Closed)
			{
				window_Map.close();
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
			map.updateCenter(--mapX, mapY);
			system("cls");
			map.printRootInfo();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
			map.updateCenter(++mapX, mapY);
			system("cls");
			map.printRootInfo();
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
			map.updateCenter(mapX, --mapY);
			system("cls");
			map.printRootInfo();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
			map.updateCenter(mapX, ++mapY);
			system("cls");
			map.printRootInfo();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
			map.updateCenter(0, 0);
			mapX = 0, mapY = 0;
			system("cls");
			map.printRootInfo();
		}

		window_Map.clear();


		map.draw(window_Map);


		window_Map.display();
	}
		

	return 0;
}